angular.module('canapp', ['ui.bootstrap'])
    .config(function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    })
    .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');
    })

    /* Система оповещений */
    .controller('globalMessages', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {
        $scope.alerts = [];

        $rootScope.addAlert = function(type, msg) {
            $scope.alerts.push({'type': type, 'msg': msg});
        };

        $rootScope.closeAlert = function(alert) {
            $rootScope.closeAlertByIndex($scope.alerts.indexOf(alert));
        };

        $rootScope.closeAlertByIndex = function(index) {
            $scope.alerts.pop(index);
        };

    }])

    .controller('OrderCalculator', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {

        $scope._a_price = 0;  // Стоимость на одного человека
        $scope._a_weight = 0;  // Вес на одного человека

        $scope.defaults = {
            'order': {
                id: null,
                cost: 0,
                title: 'Мой заказ',
                number: null,
                dishes: [],
                person_num: 1,
            },
            'api_name': '',
        }

        $scope.order = $scope.defaults.order;
        $scope.groups = [];
        $scope.dishes = [];
        $scope.servings = [];
        $scope.displayableDishes = [];
        $scope.totalCount = 1;
        $scope.totalCost = 0;

        $scope.init = function() {
            $scope._disabled = false;
            $scope.order = $scope.defaults.order;
        }

        $scope.api_url = function() {
            return '/api/' + $scope.defaults.api_name + '/';
        }

        $scope.reset = function() {
            $scope.init();
        }

        /* Отслеживаем изменения в заказе */
        $scope.$watch('order', function(){
            $scope.personCost = $scope.getTotalCost();
            $scope.personCount = $scope.getTotalCount();
            $scope.personWeight = $scope.getTotalWeight();
            $scope.totalCost = (!$scope.order.person_num) ? $scope.personCost : $scope.personCost * $scope.order.person_num;
            $scope.totalCount = (!$scope.order.person_num) ? $scope.personCount : $scope.personCount * $scope.order.person_num;
            $scope.totalWeight = (!$scope.order.person_num) ? $scope.personWeight : $scope.personCWeight* $scope.order.person_num;
        }, true);

        /* Получение групп блюд */
        $scope.getGroups = function() {
            return $http.get('/api/groups/').then(function(response) {
                return $scope.groups = response.data;
            });
        };

        /* Получение видов сервировки */
        $scope.getServings = function() {
            return $http.get('/api/servings/').then(function(response) {
                return $scope.servings = response.data;
            });
        };

        /* Получение списка доступных блюд */
        $scope.getDishes = function() {
            return $http.get('/api/dishes/').then(function(response) {
                return $scope.dishes = response.data;
            });
        };

        /* Получаем общую стоимость заказа */
        $scope.getTotalCost = function() {
            total_cost = 0;
            _.each($scope.order.dishes, function(dish) {
                total_cost += dish.qty * dish.__extra__.price;
            });
            return total_cost;
        };

        /* Получаем общую стоимость заказа */
        $scope.getTotalCount = function() {
            total_count = 0;
            _.each($scope.order.dishes, function(dish){
                total_count += dish.qty;
            })
            return total_count;
        }

        /* Получаем общий вес заказа */
        $scope.getTotalWeight = function() {
            total_weight = 0;
            _.each($scope.order.dishes, function(dish) {
                total_weight = dish.qty * dish.__extra__.total_weight;
            });
            return total_weight;
        };

        /* Добавляем блюдо в заказ */
        $scope.orderDish = function(dish) {
            var ix = _.findIndex($scope.order.dishes, function(el) { return el.dish == dish.id });
            if(ix != -1) {
                $scope.order.dishes[ix].qty += 1;
            } else {
                $scope.order.dishes.push({
                    __extra__: dish,
                    dish: dish.id,
                    qty: 1
                });
            }
        };

        /* Исключаем блюдо из заказа */
        $scope.unorderDish = function(dish) {
            var ix = _.findIndex($scope.order.dishes, function(el) { return el.dish == dish.id });
            if(ix != -1) {
                $scope.order.dishes[ix].qty -= 1;

                if ($scope.order.dishes[ix].qty == 0) {
                    $scope.order.dishes.splice(ix, 1);
                }
            }
        };

        /* Добавляем пожирателя */
        $scope.addPerson = function() {
            $scope.order.person_num += 1;
        }

        /* Добавляем пожирателя */
        $scope.remPerson = function() {
            if ($scope.order.person_num > 1) {
                $scope.order.person_num -= 1;
            }
        }

        $scope.orderResponseCallback = function(response) {
            $scope.order = response.data[0];
            $scope.order.cost = parseFloat($scope.order.cost);
        }

        /* Получение данных для текущего заказа */
        $scope.getOrderData = function(callback) {
            return $http.get($scope.api_url()).then(function(response){
                if (response.data.length != 0) {
                    $scope.orderResponseCallback(response);
                    $scope.order.dishes.forEach(function(el, ix){
                        $scope.order.dishes[ix] = {
                            __extra__: _.findWhere($scope.dishes, {id: el.dish}),
                            // serving: _.findWhere($scope.servings, {name: el.serving}),
                            dish: el.id,
                            qty: el.qty
                        }
                    });
                    return $scope.order
                };
            });
        };

        $scope.saveOrder = function() {
            $scope._disabled = true;
            var order = _.cloneDeep($scope.order);
            order.dishes.forEach(function(el) {
                delete el.__extra__;
            });
            $http.post('/api/orders/', order)
                .success(function(response) {
                    $scope._disabled = false;
                    $scope.order = response;
                    $rootScope.addAlert('success', 'Заказ №' + $scope.order.number + ' успешно ' + (($scope.order.id) ? 'обновлен' : 'сохранен') +'. ');
                })
                .error(function(response) {
                    $scope._disabled = true;
                    $rootScope.addAlert('danger', 'При оформлении заказа возникла ошибка');
                });
        }

        /* Оформляем вывод данных на экран */
        $scope.render = function() {
            $scope.displayableDishes = _.groupBy($scope.dishes, 'group');
            $('.tpl-data').hide();
            $('.ang-data').show();
        }

        /* Собираем все данные для отрисовки страницы */
        $scope.getData = function() {
            $scope.getGroups()
            .then($scope.getServings)
            .then($scope.getDishes)
            .then($scope.render)
            .then($scope.getOrderData)
            .then(null, function() {
                console.error('Ошибка при получении данных');
            });
        };

        $scope.getData();
    }])

    /* Контроллер заказа из меню */
    .controller('OrderMenuController', ['$scope', '$rootScope', '$http', '$controller', function($scope, $rootScope, $http, $controller) {

        angular.extend(this, $controller('OrderCalculator', {$scope: $scope}));
        $scope.defaults.api_name = 'orders';

    }])

    /* Контроллер заказа готового предложения */
    .controller('OrderProductController', ['$scope', '$rootScope', '$http', '$controller', function($scope, $rootScope, $http, $controller) {

        angular.extend(this, $controller('OrderCalculator', {$scope: $scope}));
        $scope.defaults.api_name = 'products';
        $scope.product = {id: null};

        $scope.api_url = function() {
            return '/api/' + $scope.defaults.api_name + '/' + $scope.product.id + '/';
        };

        $scope.init = function(product_id) {
            $scope._disabled = false;
            $scope.product.id = product_id;
            $scope.order = $scope.defaults.order;
            $('.tpl-data').hide();
        };

        $scope.orderResponseCallback = function(response) {
            $scope.order = response.data;
            _.each($scope.order.dishes, function(el){
                el.id = el.dish;
            });
            $scope.order.cost = parseFloat($scope.order.total_cost);
        };

    }])
