# -*- coding: utf-8 -*-
from PIL import ImageFilter
from sorl.thumbnail.engines.pil_engine import Engine

class CanapetEngine(Engine):
    """
    Расширяем функционал sorl для обработки изображений
    """
    
    def create(self, image, geometry, options):
        image = super(CanapetEngine, self).create(image, geometry, options)
        return self.use_filters(image, geometry, options)

    def use_filters(self, image, geometry, options):
#        if 'blur' in options:
#            image = image.filter(ImageFilter.GaussianBlur(radius=2))
        return image