# -*- coding: utf-8 -*-
import os
import re
import random

from banquet.models import *

def import_menu(safe=True, filedata=None, random_price=False):
    """
    Импортируем меню из текстового файла
    """
    result_log = []
    dish_group = None
    dish_types = []

    if filedata is None:
        filepath = input("Укажите путь до текстового файла меню: ")
        if '/' not in filepath:
            filepath = os.path.join(os.path.dirname(__filename__), filepath)
    
        filedata = fopen(filepath).readlines()

    if safe is False:
        Dish.objects.all().delete()
        DishGroup.objects.all().delete()

    for ln in filedata:
        ln = ln.decode('utf-8', 'ignore').strip()
        if ln:
            if ln[0] == '@':
                # Создаем новую группу блюд
                dish_group, created = DishGroup.objects.get_or_create(title=ln[1:])
            elif ln[0] == '#':
                # Создаем новую группу блюд
                dish_types = []
                for tn in ln[1:].split(','):
                    tag, created = IngredientType.objects.get_or_create(title=tn.strip())
                    dish_types.append(tag)
            elif dish_group is None:
                # Если группа не задана, вываливаемся по ошибке
                raise ValueError('A dish group (begins with #) must be defined before dish')
            else:
                # Создаем блюдо с привязкой к текущей группе блюд
                # Ищем тег блюда, определяющий тип подачи
                title, desc = ln.split('/') if '/' in ln else (ln, None)

                re_tag = re.search(r'#(\w+)', desc or title)
                serving = re_tag.group(0)[1:] if re_tag else None

                price = 0 if random_price is False else random.randint(100, 1000)
                dish = Dish.objects.create(
                    group=dish_group,
                    title=title.strip(),
                    desc=desc,
                    price=price,
                    serving=serving,
                )

                if dish_types:
                    for dt in dish_types:
                        dish.dish_types.add(dt)
                    dish.save()
                
                result_log.append(dish)

            print(ln)

    return result_log
