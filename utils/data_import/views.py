# -*- coding: utf-8 -*-
from django import http
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView

from . import import_menu


class ImportView(TemplateView):
    """
    Импорт данных через форму
    """
    template_name = 'import.html'

    def post(self, request):
        if not request.FILES or 'menu' not in request.FILES:
            raise ValueError('Menu file is required')

        result = import_menu(False, request.FILES['menu'].read().split('\n'), True)
        self.render(self.template_name, {'result': result})
