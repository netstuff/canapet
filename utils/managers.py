# -*- coding: utf-8 -*-
from django.db import models


class OnlyActives(models.Manager):
    """
    Выводим только активные записи
    """

    def get_queryset(self):
        return super(OnlyActives, self).get_queryset().filter(is_active=True)
