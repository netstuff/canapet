# -*- coding: utf-8 -*-
import os
import uuid

from banquet.settings import BASE_DIR

def upload_as_uuid(instance, filename):
    """
    Динамическое имя загружаемого файла
    """
    uuid_name = uuid.uuid4()
    module_name = instance.__class__.__name__.lower()
    return os.path.join(BASE_DIR, 'media', module_name, str(uuid_name), filename)


def upload_as_template(instance, filename):
    """
    Динамическое имя загружаемого файла
    """
    return os.path.join(BASE_DIR, 'media', 'templates', filename)
