# -*- coding: utf-8 -*-
import csv

from banquet.models import *


def csv_menu(safe=False, f=None):
    """
    Импортируем меню из текстового файла
    """
    log_messages = []

    dish_data = []
    dish_sets = set()
    dish_groups = set()
    event_types = set()
    ingredients = set()

    for ix, row in enumerate(csv.reader(f, delimiter=';')):

        if ix == 0 or len(list(filter(bool, row))) < 6:
            continue

        title, group, event, output, price, str_ingridients = [x.strip() for x in row]
        ing_set = set(x.strip() for x in str_ingridients.split(','))

        # Заполняем справочники
        dish_groups.add(group)
        event_types.add(event)
        ingredients |= ing_set

        # Добавляем блюдо в общий список
        dish_data.append({
            'title': title,
            'group': group,
            'event': event,
            'price': int(price),
            'ingredients': list(filter(bool, ing_set)),
            'total_weight': sum(list(filter(bool, [int(x) for x in output.split('/')]))),
        })

    if not safe:
        Dish.objects.all().delete()
        DishSet.objects.all().delete()
        DishGroup.objects.all().delete()
        EventType.objects.all().delete()
        Ingredient.objects.all().delete()

    event_types = {x.title: x for x, _ in [EventType.objects.get_or_create(title=t) for t in event_types]}
    dish_groups = {x.title: x for x, _ in [DishGroup.objects.get_or_create(title=t) for t in dish_groups]}
    ingredients = {x.title: x for x, _ in [Ingredient.objects.get_or_create(title=t) for t in ingredients]}

    dishes = []
    for data in dish_data:
        _g = data.pop('group')
        _e = data.pop('event')
        _i = data.pop('ingredients')

        dish = Dish(**data)
        dish.group = dish_groups.get(_g)
        dish.save()

        for __i in _i:
            dish.ingredients.add(ingredients.get(__i))

        for __e in _e:
            if __e in event_types:
                dish.events.add(event_types.get(__e))

        dish.save()

    return dishes
