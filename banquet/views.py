# -*- coding: utf-8 -*-
import os
import settings

from django.db.models import Q
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from annoying.functions import get_object_or_None
from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap

from models import *
from utils.importer import csv_menu


sitemaps = {
    'pages': FlatPageSitemap,
    'dishes': GenericSitemap({'queryset': Dish.objects.all()}),
    'products': GenericSitemap({'queryset': Product.objects.all()}),
    'collections': GenericSitemap({'queryset': Collection.objects.all()}),
}


STATIC_STYLE = lambda x: '/static/styles/{}.less'.format(x)


class UserSessionMixin:

    def get_user_session(self, request):
        session = get_object_or_None(Session, session_key=request.session.session_key)
        user = request.user if request.user.id else None
        return session, user


class ImportView(TemplateView):
    """
    Импорт данных через форму
    """
    template_name = 'import.html'

    def post(self, request):
        if not request.FILES or 'menu' not in request.FILES:
            raise ValueError('Menu file is required')

        result = csv_menu(False, request.FILES['menu'].file)
        self.render(self.template_name, {'result': result})


class CommonPage(TemplateView):
    """
    Стандартная страница сайта
    """

    def get_context_data(self, **kwargs):
        context = super(CommonPage, self).get_context_data(**kwargs)

        context['slug'] = os.path.splitext(self.template_name)[0] if self.template_name else self.kwargs.get('slug')
        context['events'] = EventType.objects.filter(is_active=True)
        context['products'] = Product.objects.filter(is_active=True).order_by('-priority')
        context['lib_servings'] = LIBRARY_TAGS_SERVING

        context['style_path'] = STATIC_STYLE(context['slug']) if os.path.isfile(os.path.join(settings.BASE_DIR, STATIC_STYLE(context['slug'])[1:])) else STATIC_STYLE('base')

        self.session = get_object_or_None(Session, session_key=self.request.session.session_key)
        context['orders'] = Order.objects.filter(Q(customer__user__pk=self.request.user.id or None) | Q(session=self.session), status__in=list(Order.STATUSES.keys()))

        return context


class StaticPage(CommonPage):
    """
    Статические страницы
    """

    def get_template_names(self):
        return ['%s.html' % self.kwargs.get('slug')]


class IndexPage(CommonPage):
    """
    Стартовая страница
    """
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexPage, self).get_context_data(**kwargs)
        context['collections'] = Collection.objects.filter()
        return context


class OrderPage(UserSessionMixin, IndexPage):
    """
    Заказ
    """
    template_name = 'order.html'

    def get_context_data(self, **kwargs):
        context = super(OrderPage, self).get_context_data(**kwargs)
        context['ingredient_types'] = IngredientType.objects.filter(is_active=True)
        context['dish_ingredients'] = Ingredient.objects.filter()
        context['dish_groups'] = DishGroup.objects.filter(is_active=True)
        context['customer'] = get_object_or_None(Customer, user__pk=self.request.user.id)
        context['order'] = Order.objects.get(Q(customer__user__pk=self.request.user.id or None) | Q(session=self.session), number=kwargs['number'])
        return context

    def post(self, request, number, status=None):
        if number:
            session, user = self.get_user_session(request)
            order = get_object_or_404(Order, Q(customer__user__pk=user.id or None) | Q(session=session), number=number)
            if order and status in Order.STATUSES:
                order.status = status
                order.save()
                return self.get(request, number=number)

class MenuPage(CommonPage):
    """
    Полное меню
    """
    template_name = 'menu.html'

    def get_context_data(self, **kwargs):
        context = super(MenuPage, self).get_context_data(**kwargs)
        context['dish_groups'] = DishGroup.objects.filter(is_active=True)
        return context


class DishPage(CommonPage):
    """
    Страница блюда
    """
    template_name = 'dish.html'

    def get_context_data(self, **kwargs):
        context = super(DishPage, self).get_context_data(**kwargs)
        context['dish'] = get_object_or_404(Dish, pk=kwargs.get('pk'), is_active=True)
#       context['also'] = OrderedDishes.objects.filter(serving=dish.serving).exclude(pk=dish.pk)
        context['prod'] = Product.objects.filter(dishes__in=[context['dish'].pk])
        return context


class ProductList(CommonPage):
    """
    Страница готовых предложений
    """
    template_name = 'products.html'


class ProductDetails(CommonPage):
    """
    Страница готового предложения
    """
    template_name = 'product.html'

    def get_context_data(self, **kwargs):
        context = super(ProductDetails, self).get_context_data(**kwargs)
        context['product'] = get_object_or_404(Product, pk=kwargs.get('pk'), is_active=True)
        return context
