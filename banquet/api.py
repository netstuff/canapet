# -*- coding: utf-8 -*-
import uuid
import datetime
import settings

from django.core import serializers
from django.http import HttpResponse
from django.db.models import Q
from django.shortcuts import get_object_or_404

from restless.models import serialize
from restless.modelviews import ListEndpoint, DetailEndpoint
from annoying.functions import get_object_or_None

from models import *
from views import UserSessionMixin


class ListDishGroups(ListEndpoint):
    """
    Список блюд
    """
    def get(self, request):
        return serialize(
            DishGroup.objects.filter(is_active=True),
            exclude=['is_active'],
        )


class LibServings(ListEndpoint):
    """
    """
    def get(self, request):
        return [dict(zip(('name', 'value'), (el))) for el in LIBRARY_TAGS_SERVING.items()]


class DishList(ListEndpoint):
    """
    Список блюд
    """
    def get(self, request):
        return serialize(
            set(Dish.objects.filter(is_active=True)) | set(DishSet.objects.filter(is_active=True)),
            exclude=['is_active'],
            include=[
                ('str_ingredients', lambda x: x.str_ingredients()),
                ('str_exit', lambda x: x.str_exit()),
            ]
        )


class DishDetails(DetailEndpoint):
    """
    Карточка блюда
    """
    def get(self, request, pk):
        return serialize(
            get_object_or_404(Dish, pk=pk, is_active=True),
            include=[
                ('ingredients', lambda x: serialize(DishIngredients.objects.filter(dish__pk=x.pk).values(
                    'ingredient__pk',
                    'ingredient__title',
                    'ingredient_weight'
                )))
            ]
        )


class ProductDetails(DetailEndpoint):
    """
    Готовое предложение
    """
    def get(self, request, pk):
        product = get_object_or_404(Product, pk=pk, is_active=True)
        result = serialize(
            product,
            include=[
                ('dishes', lambda x: serialize(InProduct.objects.filter(product__pk=x.pk))),
            ]
        )
        result.update({
            'cost': product.total_cost,
            # 'weight': product.total_weight,
        })
        return result


class Orders(UserSessionMixin, ListEndpoint):
    """
    Заказы
    """

    def get(self, request):
        session, user = self.get_user_session(request)
        return serialize(
            Order.objects.filter(Q(customer__user__pk=user.id if user else None) | Q(session=session), status__in=list(Order.STATUSES.keys())),
            include=[
                ('dishes', lambda x: serialize(InOrder.objects.filter(order__pk=x.pk))),
            ]
        )

    def post(self, request):
        """
        Создание заказа
        """
        try:
            session, user = self.get_user_session(request)
            customer = get_object_or_None(Customer, user__pk=user.pk) if user else None

            order = Order.objects.create(
                session=Session.objects.get(session_key=request.session.session_key),
                customer=customer,
                title=request.data.get('title', 0),
                person_num=int(request.data.get('persons', 0))
            )

            one_person_cost = 0
            for el_dish in request.data.get('dishes', []):
                dish_id, qty = el_dish.values()
                dish = Dish.objects.get(id=dish_id)
                InOrder.objects.create(order=order, dish=dish, qty=int(qty))

            order.save()

            dishes = order.dishes.all()
            return serialize(
                order,
                include=[
                    ('dishes', lambda x: serialize(InOrder.objects.filter(order__pk=x.pk))),
                ]
            )
        except Exception as e:
            import traceback; traceback.print_exc();
            import pdb; pdb.set_trace()
            pass

    def put(self, request, order_id):
        """
        Редактирование заказа
        """
        session, user = self.get_user_session(request)
