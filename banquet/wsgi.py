"""
WSGI config for banquet project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""
import os, sys, site

# Activate virtual enviroment
virtual_env = os.path.expanduser('~/virtualenvs/banquet')
# activate_this = os.path.join(virtual_env, 'bin/activate_this.py')
# execfile(activate_this, dict(__file__=activate_this))
site.addsitedir(virtual_env + '/lib/python2.7/site-packages')

# Add a paths
sys.path.insert(0, os.path.join(os.path.expanduser('~'), 'banquet'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "banquet.settings")

# Run, Forest, run!
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
