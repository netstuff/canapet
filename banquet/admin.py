#-*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class DishIngredientsInline(admin.TabularInline):
    model = DishIngredients
    extra = 1


class DishSetItemInline(admin.TabularInline):
    model = DishSetItem
    extra = 1


class ProductDishesInline(admin.TabularInline):
    model = InProduct
    fields = ('dish', 'qty')
    extra = 1


class OrderDishesInline(admin.TabularInline):
    model = InOrder
    fields = ('dish', 'qty')
    extra = 1


class DishAdmin(admin.ModelAdmin):
    inlines = (DishIngredientsInline,)
    list_display = ('__unicode__', 'str_ingredients', 'str_exit', 'str_weight', 'str_price', 'is_active')
    list_filter = ('group',)
    search_fields = ('title',)


class DishSetAdmin(admin.ModelAdmin):
    inlines = (DishSetItemInline,)
    list_display = ('__unicode__', 'str_weight', 'str_price', 'is_active')
    list_filter = ('group',)
    search_fields = ('title',)


class OrderAdmin(admin.ModelAdmin):
    inlines = (OrderDishesInline,)
    list_display = ('__unicode__', 'number', 'customer', 'count_dishes', 'cost', 'str_status')
    list_filter = ('status', 'collection')
    search_fields = ('number', 'customer__user', 'dish__title')


class ProductAdmin(admin.ModelAdmin):
    inlines = (ProductDishesInline,)
    list_display = ('__unicode__', 'event', 'count_dishes', 'person_num')
    list_filter = ('event',)
    search_fields = ('dish__title',)


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('str_name', 'phone')
    search_fields = ('str_name', 'phone')


admin.site.register(Page)
admin.site.register(Collection)
admin.site.register(Ingredient)
admin.site.register(DishGroup)
admin.site.register(Dish, DishAdmin)
admin.site.register(DishSet, DishSetAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(EventType)
