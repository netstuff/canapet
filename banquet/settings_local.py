DEBUG = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'canapetdb',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': '8889',
        'OPTIONS': {
            'init_command': 'SET storage_engine=MyISAM',
            'charset': 'utf8',
        }
    }
}


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
   }
}


FACEBOOK_APP_ID = ''
FACEBOOK_API_SECRET = ''

TWITTER_CONSUMER_KEY = '-'
TWITTER_CONSUMER_SECRET = '-'

VK_APP_ID = VKONTAKTE_APP_ID = '-'
VK_API_SECRET = VKONTAKTE_APP_SECRET = '-'

GOOGLE_OAUTH2_CLIENT_ID = '-'
GOOGLE_OAUTH2_CLIENT_SECRET = '-'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# EMAIL_USE_TLS = True
# EMAIL_HOST = 'smtp.yandex.ru'
# EMAIL_PORT = 465
# EMAIL_HOST_USER = 'robot@canapet.ru'
# EMAIL_HOST_PASSWORD = 'roboboogie2527'
# DEFAULT_FROM_EMAIL = 'robot@canapet.ru'
