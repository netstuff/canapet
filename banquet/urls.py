# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin

import api
from views import *

admin.autodiscover()

urlpatterns = patterns('',

    # Динамические страницы:
    url(r'^$', IndexPage.as_view()),
    url(r'^kitchen/', include(admin.site.urls)),
    url(r'^import/$', ImportView.as_view()),
    url(r'^orders/$', OrderPage.as_view()),
    url(r'^orders/(?P<number>\w+)/$', OrderPage.as_view()),
    url(r'^orders/(?P<number>\w+)/(?P<status>%s)/$' % '|'.join(Order.STATUSES.keys()), OrderPage.as_view()),
    url(r'^menu/$', MenuPage.as_view()),
    url(r'^dishes/(?P<pk>\d+)/$', DishPage.as_view()),
    url(r'^products/$', ProductList.as_view()),
    url(r'^products/(?P<pk>\d+)/$', ProductDetails.as_view()),
    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),

    # API
    url(r'^api/groups/$', api.ListDishGroups.as_view()),
    url(r'^api/servings/$', api.LibServings.as_view()),
    url(r'^api/dishes/$', api.DishList.as_view()),
    url(r'^api/dishes/(?P<pk>\d+)/$', api.DishDetails.as_view()),
    url(r'^api/products/(?P<pk>\d+)/$', api.ProductDetails.as_view()),
    url(r'^api/orders/$', api.Orders.as_view()),

    # Все остальное
    url(r'^(?P<slug>\w+)/$', StaticPage.as_view()),
)


if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
