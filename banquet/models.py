# -*- coding: utf-8 -*-
import random
import string

from decimal import Decimal
from collections import OrderedDict

from django.db import models
from django.dispatch import receiver
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save, post_save
from django.contrib.sessions.models import Session
from sorl.thumbnail import ImageField

from utils.managers import OnlyActives
from utils.files import *


LIBRARY_TAGS_SERVING = {
    'canape': 'канапе',
    'roll': 'ролл',
    'short': 'в шоте',
    'skewer': 'на шпажке',
    'spoon': 'на ложечке',
    'tartlet': 'тарталетка',
    'volauvent': 'в волаване',
}


def generate_number(n=None):
    """
    Генерация номера заказа
    """
    n = n or 8
    return ''.join(random.SystemRandom().choice(string.uppercase + string.digits) for _ in xrange(n)).upper()


class CatalogueItem(models.Model):
    """
    Позиция в каталоге
    """
    title = models.CharField(max_length=255, verbose_name=u'заголовок')
    desc = models.TextField(blank=True, null=True, verbose_name=u'описание')
    image = models.ImageField(upload_to=upload_as_uuid, blank=True, verbose_name=u'фото')
    price = models.PositiveIntegerField(verbose_name=u'цена', help_text=u'руб.')

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title

    def str_price(self):
        return '%s руб.' % self.price

    def str_weight(self):
        return '%s гр.' % self.get_total_weight()

    def get_absolute_url(self):
        return reverse('%s.views.details' % self.__class__.__name__.lower(), args=[str(self.id)])

    str_price.short_description = 'Цена'
    str_weight.short_description = 'Вес'


class Page(models.Model):
    """
    Раздел сайта
    """
    title = models.CharField(max_length=50, unique=True, verbose_name=u'заголовок страницы')
    slug = models.SlugField(blank=False, verbose_name=u'короткое имя')
    html = models.TextField(blank=True, verbose_name=u'текст страницы')
    image = models.ImageField(upload_to=upload_as_uuid, blank=True, verbose_name=u'изображение')
    redirect = models.URLField(blank=True, verbose_name=u'переадресация')
    tpl_file = models.FileField(upload_to=upload_as_template, blank=True, verbose_name=u'шаблон страницы')
    is_active = models.BooleanField(default=True, verbose_name=u'доступен на сайте')
    queue = models.PositiveIntegerField(default=0, verbose_name=u'порядок сортировки')

    class Meta:
        verbose_name = u'страница сайта'
        verbose_name_plural = u'страницы сайта'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return '/%s/' % self.slug


class Collection(models.Model):
    """
    Коллекция (стиль накрытия)
    """
    title = models.CharField(max_length=255, verbose_name=u'заголовок')
    desc = models.TextField(blank=True, null=True, verbose_name=u'описание')
    image = models.ImageField(upload_to=upload_as_uuid, blank=True, verbose_name=u'фото')

    class Meta:
        verbose_name = u'коллекция'
        verbose_name_plural = u'коллекции'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return '/collections/%d/' % self.id


class EventType(models.Model):
    """
    Тип мероприятия
    """
    title = models.CharField(max_length=50, unique=True, verbose_name=u'заголовок мероприятия')
    image = models.ImageField(upload_to=upload_as_uuid, blank=True, verbose_name=u'изображение')
    html = models.TextField(blank=True, verbose_name=u'текст страницы')
    added = models.DateTimeField(auto_now_add=True, verbose_name=u'дата создания')
    changed = models.DateTimeField(auto_now=True, verbose_name=u'дата изменения')
    is_active = models.BooleanField(default=True, verbose_name=u'доступен на сайте')

    class Meta:
        verbose_name = u'мероприятие'
        verbose_name_plural = u'типы мероприятий'

    def __unicode__(self):
        return self.title


class IngredientType(models.Model):
    """
    Тип ингридиента
    """
    title = models.CharField(max_length=50, unique=True)
    is_active = models.BooleanField(default=True, verbose_name=u'доступно на сайте')

    class Meta:
        verbose_name = u'тип блюда'
        verbose_name_plural = u'типы блюд по содержанию'

    def __unicode__(self):
        return self.title


class Ingredient(models.Model):
    """
    Ингредиент блюда
    """
    title = models.CharField(max_length=100, unique=True, verbose_name=u'заголовок')

    class Meta:
        verbose_name = u'ингредиент'
        verbose_name_plural = u'справочник ингредиентов'

    def __unicode__(self):
        return self.title


class DishGroup(models.Model):
    """
    Группа блюд (холодные закуски, горячее, десерты и т.п.)
    """
    parent = models.ForeignKey('self', blank=True, null=True, verbose_name=u'входит в')
    title = models.CharField(max_length=255, unique=True, verbose_name=u'заголовок')
    desc = models.TextField(blank=True, verbose_name=u'описание')
    is_active = models.BooleanField(default=True, verbose_name=u'доступно на сайте')

    class Meta:
        verbose_name = u'группа блюд'
        verbose_name_plural = u'справочник групп блюд'

    def __unicode__(self):
        return self.title


class Dish(CatalogueItem):
    """
    Блюдо
    """
    group = models.ForeignKey('DishGroup', related_name='dishes', verbose_name=u'группа блюд')
    events = models.ManyToManyField('EventType', blank=True, related_name='dishes', verbose_name=u'тип мероприятия')
    serving = models.CharField(max_length=32, default=None, null=True, blank=True, choices=LIBRARY_TAGS_SERVING.items(), verbose_name=u'подача')
    dish_types = models.ManyToManyField('IngredientType', blank=True, verbose_name=u'относится к блюдам типа')
    ingredients = models.ManyToManyField('Ingredient', through='DishIngredients', verbose_name=u'ингредиенты')
    total_weight = models.PositiveIntegerField(default=0, verbose_name=u'общий вес', help_text=u'грамм')
    is_active = models.BooleanField(default=True, verbose_name=u'вкл.')

    class Meta:
        ordering = ['title']
        verbose_name = u'блюдо'
        verbose_name_plural = u'блюда'

    def get_total_weight(self):
        return self.total_weight if self.total_weight else sum(DishIngredients.objects.filter(dish__id=self.id).values_list('ingredient_weight', flat=True))

    def str_ingredients(self):
        return ', '.join([x.title.lower() for x in self.ingredients.all()])

    def str_exit(self):
        return '/'.join([str(x) for x in DishIngredients.objects.filter(dish__id=self.id).values_list('ingredient_weight', flat=True) if x])

    def str_serving(self):
        return LIBRARY_TAGS_SERVING.get(self.serving, '')

    def qty_ordered(self):
        return sum(x.qty for x in self.ordereddishes_set.all())

    def get_absolute_url(self):
        return '/dishes/%d/' % self.id

    str_ingredients.short_description = 'Ингредиенты'
    str_exit.short_description = 'Выход'


class DishSetItem(models.Model):
    """
    Блюдо, закрепленное за сетом
    """
    dish = models.ForeignKey('Dish', verbose_name=u'блюдо')
    dish_set = models.ForeignKey('DishSet', verbose_name=u'сет')
    dish_qty = models.PositiveIntegerField(default=1, verbose_name=u'количество блюд в сете', help_text=u'шт')


class DishSet(CatalogueItem):
    """
    Набор блюд
    """
    group = models.ForeignKey('DishGroup', related_name='dishsets', verbose_name=u'группа блюд')
    dishes = models.ManyToManyField('Dish', through='DishSetItem', verbose_name=u'включает блюда')
    is_active = models.BooleanField(default=True, verbose_name=u'вкл.')

    class Meta:
        ordering = ['title']
        verbose_name = u'Блюдо-сет'
        verbose_name_plural = u'Блюдо-сеты'

    def get_total_weight(self):
        items_qty = {x.dish.id: x.dish_qty for x in self.dishsetitem_set.all()}
        return sum((x.get_total_weight() * items_qty.get(x.id, 0))  for x in self.dishes.all())

    def str_ingredients(self):
        ingredients = set()
        for dish in self.dishes.all():
            ingredients |= set(dish.ingredients.all())
        return ', '.join([x.title.lower() for x in ingredients])

    def str_exit(self):
        return '/'.join([str(x.total_weight) for x in self.dishes.all()])


class DishIngredients(models.Model):
    """
    Ингредиенты блюда
    """
    dish = models.ForeignKey('Dish', verbose_name=u'блюдо')
    ingredient = models.ForeignKey('Ingredient', verbose_name=u'ингредиент')
    ingredient_weight = models.PositiveIntegerField(default=0, verbose_name=u'вес ингредиента', help_text=u'грамм')

    class Meta:
        # auto_created = True
        verbose_name = u'ингредиент блюда'
        verbose_name_plural = u'ингредиенты блюда'

    def __unicode__(self):
        return u'%s в %s' % (self.ingredient.title, self.dish.title)


class DishBind(models.Model):
    """
    Привязка блюда
    """
    dish = models.ForeignKey('Dish', blank=False, verbose_name=u'блюдо')
    qty = models.PositiveIntegerField(default=1, verbose_name=u'количество')

    class Meta:
        abstract = True
        verbose_name = u'связь с блюдом'
        verbose_name_plural = u'прикоепленные блюда'

    def __unicode__(self):
        return ''

    @property
    def cost(self):
        return self.dish.price * self.qty


class InProduct(DishBind):
    """ Блюдо в продукте """
    product = models.ForeignKey('Product', blank=True, null=True, verbose_name=u'готовое предложение')


class InOrder(DishBind):
    """ Блюдо в заказе """
    order = models.ForeignKey('Order', blank=True, null=True, verbose_name=u'заказ')


class OrderProduct(models.Model):
    """
    Базовый класс для формирования готового предложения и заказа
    """
    image = models.ImageField(upload_to=upload_as_uuid, blank=True, verbose_name=u'изображение')
    html = models.TextField(blank=True, verbose_name=u'текст страницы')
    added = models.DateTimeField(auto_now_add=True, verbose_name=u'дата создания')
    changed = models.DateTimeField(auto_now=True, verbose_name=u'дата изменения')
    discount = models.PositiveIntegerField(default=1, verbose_name=u'скидка')
    person_num = models.PositiveIntegerField(default=0, verbose_name=u'кол-во персон')
    custemplate = models.CharField(max_length=64, blank=True, verbose_name=u'шаблон страницы')

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title

    def count_dishes(self):
        return self.dishes.count()

    count_dishes.short_description = 'Кол-во блюд'


class Product(OrderProduct):
    """
    Готовое предложение
    """
    event = models.ForeignKey('EventType', related_name='product', verbose_name=u' тип события')
    title = models.CharField(max_length=50, unique=True, verbose_name=u'заголовок')
    dishes = models.ManyToManyField('Dish', blank=False, through='InProduct', verbose_name=u'блюда')
    slogan = models.CharField(max_length=255, blank=True, verbose_name=u'слоган')
    priority = models.PositiveIntegerField(default=0, verbose_name=u'приоритет')
    is_active = models.BooleanField(default=True, verbose_name=u'доступен на сайте')

    class Meta:
        verbose_name = u'готовое предложение'
        verbose_name_plural = u'готовые предложения'

    def __unicode__(self):
        return self.title

    @property
    def cost(self):
        return self.total_cost

    @property
    def total_cost(self):
        return sum((x.dish.price * x.qty) for x in self.inproduct_set.all()) * self.discount

    def get_absolute_url(self):
        return '/products/%d/' % self.id


class CustomUser(AbstractUser):
    pass


class Customer(models.Model):
    """
    Получаетель заказа
    """
    user = models.OneToOneField(CustomUser, verbose_name=u'пользователь')
    phone = models.CharField(max_length=10, default='', verbose_name=u'телефон')
    address = models.CharField(max_length=255, default='', verbose_name=u'адрес')
    str_name = models.CharField(max_length=255, default='', verbose_name='имя строкой')

    class Meta:
        verbose_name = u'клиент'
        verbose_name_plural = u'клиенты'


class Order(OrderProduct):
    """
    Собранный заказ
    """
    STATUSES = OrderedDict([
        ('in_progress', 'в процессе'),
        ('issued', 'оформлен'),
        # ('prepaid', 'внесена предоплата'),
        ('paid', 'оплачен'),
        ('wip', 'в работе'),
        ('ready', 'готов к доставке'),
        ('delivered', 'доставлен'),
        ('closed', 'закрыт'),
        ('denied', 'отказ'),
    ])


    title = models.CharField(max_length=50, verbose_name=u'заголовок')
    cost = models.DecimalField(max_digits=10, decimal_places=2, default=0.00, verbose_name=u'стоимость', help_text='руб')
    dishes = models.ManyToManyField('Dish', blank=False, through='InOrder', verbose_name=u'блюда')
    number = models.CharField(max_length=32, unique=True, blank=False, default=generate_number, verbose_name=u'номер заказа')
    status = models.CharField(max_length=15, choices=STATUSES.items(), default=STATUSES.keys()[0], verbose_name=u'статус')
    session = models.ForeignKey(Session, blank=False, verbose_name=u'сессия')
    customer = models.ForeignKey(Customer, blank=True, null=True, verbose_name=u'пользователь')
    collection = models.ForeignKey(Collection, blank=True, null=True, verbose_name=u'коллекция')

    class Meta:
        verbose_name = u'заказ'
        verbose_name_plural = u'заказы'

    def __unicode__(self):
        return self.title

    def count_dishes(self):
        return self.dishes.count()

    def str_status(self):
        return self.STATUSES.get(self.status, 'n/a')

    @classmethod
    def get_current_list(cls):
        return cls.objects.filter(status__not__in=['delivered', 'deny'])

    @classmethod
    def get_archive_list(cls):
        return cls.objects.filter(status=['delivered'])

    @property
    def total_cost(self):
        return sum((x.dish.price * x.qty) for x in self.inorder_set.all()) * self.discount


"""
Сигналы
"""

@receiver(pre_save, sender=Order)
def sum_order_cost(sender, instance, *args, **kwargs):
    """
    Суммируем все блюда заказа перед его сохранением
    """
    instance.cost = Decimal((instance.person_num or 1) * instance.total_cost)

@receiver(post_save, sender=Order)
def mail_notify(sender, instance, *args, **kwargs):
    """
    E-mail оповещения об изменении статуса заказа
    """
    send_mail('Test canapet mail', 'Ok!', 'robot@canapet.ru', ['ya@vauhaus.ru'])
