# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.auth.models
import django.utils.timezone
from django.conf import settings
import django.core.validators
import utils.files
import banquet.models


class Migration(migrations.Migration):

    dependencies = [
        ('sessions', '0001_initial'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, max_length=30, validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.', 'invalid')], help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, verbose_name='username')),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(max_length=254, verbose_name='email address', blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('desc', models.TextField(null=True, verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('image', models.ImageField(upload_to=utils.files.upload_as_uuid, verbose_name='\u0444\u043e\u0442\u043e', blank=True)),
            ],
            options={
                'verbose_name': '\u043a\u043e\u043b\u043b\u0435\u043a\u0446\u0438\u044f',
                'verbose_name_plural': '\u043a\u043e\u043b\u043b\u0435\u043a\u0446\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone', models.CharField(default=b'', max_length=10, verbose_name='\u0442\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('address', models.CharField(default=b'', max_length=255, verbose_name='\u0430\u0434\u0440\u0435\u0441')),
                ('str_name', models.CharField(default=b'', max_length=255, verbose_name=b'\xd0\xb8\xd0\xbc\xd1\x8f \xd1\x81\xd1\x82\xd1\x80\xd0\xbe\xd0\xba\xd0\xbe\xd0\xb9')),
                ('user', models.OneToOneField(verbose_name='\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '\u043a\u043b\u0438\u0435\u043d\u0442',
                'verbose_name_plural': '\u043a\u043b\u0438\u0435\u043d\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Dish',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('desc', models.TextField(null=True, verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('image', models.ImageField(upload_to=utils.files.upload_as_uuid, verbose_name='\u0444\u043e\u0442\u043e', blank=True)),
                ('price', models.PositiveIntegerField(help_text='\u0440\u0443\u0431.', verbose_name='\u0446\u0435\u043d\u0430')),
                ('serving', models.CharField(default=None, choices=[(b'volauvent', b'\xd0\xb2 \xd0\xb2\xd0\xbe\xd0\xbb\xd0\xb0\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb5'), (b'short', b'\xd0\xb2 \xd1\x88\xd0\xbe\xd1\x82\xd0\xb5'), (b'tartlet', b'\xd1\x82\xd0\xb0\xd1\x80\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb5\xd1\x82\xd0\xba\xd0\xb0'), (b'canape', b'\xd0\xba\xd0\xb0\xd0\xbd\xd0\xb0\xd0\xbf\xd0\xb5'), (b'spoon', b'\xd0\xbd\xd0\xb0 \xd0\xbb\xd0\xbe\xd0\xb6\xd0\xb5\xd1\x87\xd0\xba\xd0\xb5'), (b'skewer', b'\xd0\xbd\xd0\xb0 \xd1\x88\xd0\xbf\xd0\xb0\xd0\xb6\xd0\xba\xd0\xb5'), (b'roll', b'\xd1\x80\xd0\xbe\xd0\xbb\xd0\xbb')], max_length=32, blank=True, null=True, verbose_name='\u043f\u043e\u0434\u0430\u0447\u0430')),
                ('total_weight', models.PositiveIntegerField(default=0, help_text='\u0433\u0440\u0430\u043c\u043c', verbose_name='\u043e\u0431\u0449\u0438\u0439 \u0432\u0435\u0441')),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0432\u043a\u043b.')),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': '\u0431\u043b\u044e\u0434\u043e',
                'verbose_name_plural': '\u0431\u043b\u044e\u0434\u0430',
            },
        ),
        migrations.CreateModel(
            name='DishGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=255, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('desc', models.TextField(verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0434\u043e\u0441\u0442\u0443\u043f\u043d\u043e \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('parent', models.ForeignKey(verbose_name='\u0432\u0445\u043e\u0434\u0438\u0442 \u0432', blank=True, to='banquet.DishGroup', null=True)),
            ],
            options={
                'verbose_name': '\u0433\u0440\u0443\u043f\u043f\u0430 \u0431\u043b\u044e\u0434',
                'verbose_name_plural': '\u0441\u043f\u0440\u0430\u0432\u043e\u0447\u043d\u0438\u043a \u0433\u0440\u0443\u043f\u043f \u0431\u043b\u044e\u0434',
            },
        ),
        migrations.CreateModel(
            name='DishIngredients',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ingredient_weight', models.PositiveIntegerField(default=0, help_text='\u0433\u0440\u0430\u043c\u043c', verbose_name='\u0432\u0435\u0441 \u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442\u0430')),
                ('dish', models.ForeignKey(verbose_name='\u0431\u043b\u044e\u0434\u043e', to='banquet.Dish')),
            ],
            options={
                'verbose_name': '\u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442 \u0431\u043b\u044e\u0434\u0430',
                'verbose_name_plural': '\u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442\u044b \u0431\u043b\u044e\u0434\u0430',
            },
        ),
        migrations.CreateModel(
            name='DishSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('desc', models.TextField(null=True, verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('image', models.ImageField(upload_to=utils.files.upload_as_uuid, verbose_name='\u0444\u043e\u0442\u043e', blank=True)),
                ('price', models.PositiveIntegerField(help_text='\u0440\u0443\u0431.', verbose_name='\u0446\u0435\u043d\u0430')),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0432\u043a\u043b.')),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': '\u0411\u043b\u044e\u0434\u043e-\u0441\u0435\u0442',
                'verbose_name_plural': '\u0411\u043b\u044e\u0434\u043e-\u0441\u0435\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='DishSetItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dish_qty', models.PositiveIntegerField(default=1, help_text='\u0448\u0442', verbose_name='\u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0431\u043b\u044e\u0434 \u0432 \u0441\u0435\u0442\u0435')),
                ('dish', models.ForeignKey(verbose_name='\u0431\u043b\u044e\u0434\u043e', to='banquet.Dish')),
                ('dish_set', models.ForeignKey(verbose_name='\u0441\u0435\u0442', to='banquet.DishSet')),
            ],
        ),
        migrations.CreateModel(
            name='EventType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=50, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f')),
                ('image', models.ImageField(upload_to=utils.files.upload_as_uuid, verbose_name='\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('html', models.TextField(verbose_name='\u0442\u0435\u043a\u0441\u0442 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True)),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('changed', models.DateTimeField(auto_now=True, verbose_name='\u0434\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0434\u043e\u0441\u0442\u0443\u043f\u0435\u043d \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
            ],
            options={
                'verbose_name': '\u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u0435',
                'verbose_name_plural': '\u0442\u0438\u043f\u044b \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u0439',
            },
        ),
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=100, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
            ],
            options={
                'verbose_name': '\u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442',
                'verbose_name_plural': '\u0441\u043f\u0440\u0430\u0432\u043e\u0447\u043d\u0438\u043a \u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='IngredientType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=50)),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0434\u043e\u0441\u0442\u0443\u043f\u043d\u043e \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
            ],
            options={
                'verbose_name': '\u0442\u0438\u043f \u0431\u043b\u044e\u0434\u0430',
                'verbose_name_plural': '\u0442\u0438\u043f\u044b \u0431\u043b\u044e\u0434 \u043f\u043e \u0441\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u044e',
            },
        ),
        migrations.CreateModel(
            name='InOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('qty', models.PositiveIntegerField(default=1, verbose_name='\u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('dish', models.ForeignKey(verbose_name='\u0431\u043b\u044e\u0434\u043e', to='banquet.Dish')),
            ],
            options={
                'abstract': False,
                'verbose_name': '\u0441\u0432\u044f\u0437\u044c \u0441 \u0431\u043b\u044e\u0434\u043e\u043c',
                'verbose_name_plural': '\u043f\u0440\u0438\u043a\u043e\u0435\u043f\u043b\u0435\u043d\u043d\u044b\u0435 \u0431\u043b\u044e\u0434\u0430',
            },
        ),
        migrations.CreateModel(
            name='InProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('qty', models.PositiveIntegerField(default=1, verbose_name='\u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('dish', models.ForeignKey(verbose_name='\u0431\u043b\u044e\u0434\u043e', to='banquet.Dish')),
            ],
            options={
                'abstract': False,
                'verbose_name': '\u0441\u0432\u044f\u0437\u044c \u0441 \u0431\u043b\u044e\u0434\u043e\u043c',
                'verbose_name_plural': '\u043f\u0440\u0438\u043a\u043e\u0435\u043f\u043b\u0435\u043d\u043d\u044b\u0435 \u0431\u043b\u044e\u0434\u0430',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=utils.files.upload_as_uuid, verbose_name='\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('html', models.TextField(verbose_name='\u0442\u0435\u043a\u0441\u0442 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True)),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('changed', models.DateTimeField(auto_now=True, verbose_name='\u0434\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('discount', models.PositiveIntegerField(default=1, verbose_name='\u0441\u043a\u0438\u0434\u043a\u0430')),
                ('person_num', models.PositiveIntegerField(default=0, verbose_name='\u043a\u043e\u043b-\u0432\u043e \u043f\u0435\u0440\u0441\u043e\u043d')),
                ('custemplate', models.CharField(max_length=64, verbose_name='\u0448\u0430\u0431\u043b\u043e\u043d \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True)),
                ('title', models.CharField(max_length=50, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('cost', models.DecimalField(default=0.0, help_text=b'\xd1\x80\xd1\x83\xd0\xb1', verbose_name='\u0441\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c', max_digits=10, decimal_places=2)),
                ('number', models.CharField(default=banquet.models.generate_number, unique=True, max_length=32, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u0437\u0430\u043a\u0430\u0437\u0430')),
                ('status', models.CharField(default=b'in_progress', max_length=15, verbose_name='\u0441\u0442\u0430\u0442\u0443\u0441', choices=[(b'in_progress', b'\xd0\xb2 \xd0\xbf\xd1\x80\xd0\xbe\xd1\x86\xd0\xb5\xd1\x81\xd1\x81\xd0\xb5'), (b'issued', b'\xd0\xbe\xd1\x84\xd0\xbe\xd1\x80\xd0\xbc\xd0\xbb\xd0\xb5\xd0\xbd'), (b'paid', b'\xd0\xbe\xd0\xbf\xd0\xbb\xd0\xb0\xd1\x87\xd0\xb5\xd0\xbd'), (b'wip', b'\xd0\xb2 \xd1\x80\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd0\xb5'), (b'ready', b'\xd0\xb3\xd0\xbe\xd1\x82\xd0\xbe\xd0\xb2 \xd0\xba \xd0\xb4\xd0\xbe\xd1\x81\xd1\x82\xd0\xb0\xd0\xb2\xd0\xba\xd0\xb5'), (b'delivered', b'\xd0\xb4\xd0\xbe\xd1\x81\xd1\x82\xd0\xb0\xd0\xb2\xd0\xbb\xd0\xb5\xd0\xbd'), (b'closed', b'\xd0\xb7\xd0\xb0\xd0\xba\xd1\x80\xd1\x8b\xd1\x82'), (b'denied', b'\xd0\xbe\xd1\x82\xd0\xba\xd0\xb0\xd0\xb7')])),
                ('collection', models.ForeignKey(verbose_name='\u043a\u043e\u043b\u043b\u0435\u043a\u0446\u0438\u044f', blank=True, to='banquet.Collection', null=True)),
                ('customer', models.ForeignKey(verbose_name='\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', blank=True, to='banquet.Customer', null=True)),
                ('dishes', models.ManyToManyField(to='banquet.Dish', verbose_name='\u0431\u043b\u044e\u0434\u0430', through='banquet.InOrder')),
                ('session', models.ForeignKey(verbose_name='\u0441\u0435\u0441\u0441\u0438\u044f', to='sessions.Session')),
            ],
            options={
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=50, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b')),
                ('slug', models.SlugField(verbose_name='\u043a\u043e\u0440\u043e\u0442\u043a\u043e\u0435 \u0438\u043c\u044f')),
                ('html', models.TextField(verbose_name='\u0442\u0435\u043a\u0441\u0442 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True)),
                ('image', models.ImageField(upload_to=utils.files.upload_as_uuid, verbose_name='\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('redirect', models.URLField(verbose_name='\u043f\u0435\u0440\u0435\u0430\u0434\u0440\u0435\u0441\u0430\u0446\u0438\u044f', blank=True)),
                ('tpl_file', models.FileField(upload_to=utils.files.upload_as_template, verbose_name='\u0448\u0430\u0431\u043b\u043e\u043d \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0434\u043e\u0441\u0442\u0443\u043f\u0435\u043d \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('queue', models.PositiveIntegerField(default=0, verbose_name='\u043f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438')),
            ],
            options={
                'verbose_name': '\u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430 \u0441\u0430\u0439\u0442\u0430',
                'verbose_name_plural': '\u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0441\u0430\u0439\u0442\u0430',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=utils.files.upload_as_uuid, verbose_name='\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('html', models.TextField(verbose_name='\u0442\u0435\u043a\u0441\u0442 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True)),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('changed', models.DateTimeField(auto_now=True, verbose_name='\u0434\u0430\u0442\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f')),
                ('discount', models.PositiveIntegerField(default=1, verbose_name='\u0441\u043a\u0438\u0434\u043a\u0430')),
                ('person_num', models.PositiveIntegerField(default=0, verbose_name='\u043a\u043e\u043b-\u0432\u043e \u043f\u0435\u0440\u0441\u043e\u043d')),
                ('custemplate', models.CharField(max_length=64, verbose_name='\u0448\u0430\u0431\u043b\u043e\u043d \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True)),
                ('title', models.CharField(unique=True, max_length=50, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('slogan', models.CharField(max_length=255, verbose_name='\u0441\u043b\u043e\u0433\u0430\u043d', blank=True)),
                ('priority', models.PositiveIntegerField(default=0, verbose_name='\u043f\u0440\u0438\u043e\u0440\u0438\u0442\u0435\u0442')),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0434\u043e\u0441\u0442\u0443\u043f\u0435\u043d \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('dishes', models.ManyToManyField(to='banquet.Dish', verbose_name='\u0431\u043b\u044e\u0434\u0430', through='banquet.InProduct')),
                ('event', models.ForeignKey(related_name='product', verbose_name=' \u0442\u0438\u043f \u0441\u043e\u0431\u044b\u0442\u0438\u044f', to='banquet.EventType')),
            ],
            options={
                'verbose_name': '\u0433\u043e\u0442\u043e\u0432\u043e\u0435 \u043f\u0440\u0435\u0434\u043b\u043e\u0436\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0433\u043e\u0442\u043e\u0432\u044b\u0435 \u043f\u0440\u0435\u0434\u043b\u043e\u0436\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.AddField(
            model_name='inproduct',
            name='product',
            field=models.ForeignKey(verbose_name='\u0433\u043e\u0442\u043e\u0432\u043e\u0435 \u043f\u0440\u0435\u0434\u043b\u043e\u0436\u0435\u043d\u0438\u0435', blank=True, to='banquet.Product', null=True),
        ),
        migrations.AddField(
            model_name='inorder',
            name='order',
            field=models.ForeignKey(verbose_name='\u0437\u0430\u043a\u0430\u0437', blank=True, to='banquet.Order', null=True),
        ),
        migrations.AddField(
            model_name='dishset',
            name='dishes',
            field=models.ManyToManyField(to='banquet.Dish', verbose_name='\u0432\u043a\u043b\u044e\u0447\u0430\u0435\u0442 \u0431\u043b\u044e\u0434\u0430', through='banquet.DishSetItem'),
        ),
        migrations.AddField(
            model_name='dishset',
            name='group',
            field=models.ForeignKey(related_name='dishsets', verbose_name='\u0433\u0440\u0443\u043f\u043f\u0430 \u0431\u043b\u044e\u0434', to='banquet.DishGroup'),
        ),
        migrations.AddField(
            model_name='dishingredients',
            name='ingredient',
            field=models.ForeignKey(verbose_name='\u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442', to='banquet.Ingredient'),
        ),
        migrations.AddField(
            model_name='dish',
            name='dish_types',
            field=models.ManyToManyField(to='banquet.IngredientType', verbose_name='\u043e\u0442\u043d\u043e\u0441\u0438\u0442\u0441\u044f \u043a \u0431\u043b\u044e\u0434\u0430\u043c \u0442\u0438\u043f\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='dish',
            name='events',
            field=models.ManyToManyField(related_name='dishes', verbose_name='\u0442\u0438\u043f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', to='banquet.EventType', blank=True),
        ),
        migrations.AddField(
            model_name='dish',
            name='group',
            field=models.ForeignKey(related_name='dishes', verbose_name='\u0433\u0440\u0443\u043f\u043f\u0430 \u0431\u043b\u044e\u0434', to='banquet.DishGroup'),
        ),
        migrations.AddField(
            model_name='dish',
            name='ingredients',
            field=models.ManyToManyField(to='banquet.Ingredient', verbose_name='\u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442\u044b', through='banquet.DishIngredients'),
        ),
    ]
